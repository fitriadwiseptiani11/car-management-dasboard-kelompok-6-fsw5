const express = require("express");
const router = express.Router();

const { 
  findAllCars, 
  deleteCar, 
  addCar, 
  createCar, 
  editCar, 
  filterCars, 
  updateCar 
} = require("../controllers/controller");

router.get("/", (req, res) => {
  res.status(200);
  res.render("index", {
    title: "Binar Rental Car",
    layout: "dashboard",
  });
});

router.get("/api/cars/add-car", addCar);

router.get("/api/cars", findAllCars);

router.post("/api/cars/create", createCar);

router.delete("/api/cars/delete/:id", deleteCar);

router.get("/api/cars/edit/:id", editCar);

router.put("/api/cars/update/:id", updateCar);

router.get("/api/cars/search", filterCars);

module.exports = router;

router.use((req, res) => {
  res.status(404);
  res.send(`<h3>Halaman Tidak Dapat Ditemukan ! </h3>`);
});