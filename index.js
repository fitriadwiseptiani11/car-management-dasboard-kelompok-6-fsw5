const express = require("express");
const PORT = 4000;
const app = express();
const multer = require("multer");
const bodyParser = require("body-parser");
const router = require("./routes/route");

app.use(express.json());

app.set("view engine", "ejs");

app.use(express.static("public"));

const expressLayouts = require("express-ejs-layouts");
const methodOverride = require("method-override");
const session = require("express-session");
const cookieParse = require("cookie-parser");
const flash = require("connect-flash");

app.use(flash());

app.use(methodOverride("_method"));

app.use(expressLayouts);

app.use(express.urlencoded({ extended: true }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());


// config flash msg
app.use(cookieParse("secret"));
app.use(
  session({
    cookie: { maxAge: 6000 },
    secret: "secret",
    resave: true,
    saveUninitialized: true,
  })
);

// // setting image upload
// const storage = multer.diskStorage({
//   destination: (req, file, cb) => {
//     cb(null, "./public/uploads/");
//   },
//   filename: (req, file, cb) => {
//     const uniqueSuffix = Date.now() + "-" + Math.round(Math.random() * 1e9);
//     cb(null, file.fieldname + "-" + uniqueSuffix);
//   },
// });
// const upload = multer({ storage: storage });

// //use cors
// app.use(cors());

// //Testing database connection
// try {
//   await db.authenticate();
//   console.log('Connection has been established successfully.');
// } catch (error) {
//   console.error('Unable to connect to the database')
// }

app.use("/", router);

app.listen(PORT, () => {
  console.log(`app listening on port ${PORT}`);
});
