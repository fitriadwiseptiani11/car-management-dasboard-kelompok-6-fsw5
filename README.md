# Car Management Dasboard-Kelompok 6-FSW5
// Kelompok 6

- DB Diagram
  <br>
  https://dbdiagram.io/d/6262c7481072ae0b6ad13316
  <img src="ERD/ERD_Car Management Dashboard.png" alt="Page" width="250" height="300">


- Page Dashboard
  <br>
  <img src="public/images/capture/dashboard.png">

- Page Cars
  <br>
  <img src="public/images/capture/cars.png">

- Page Filter Cars Size
  <br>
  <img src="public/images/capture/small.png">
  <img src="public/images/capture/medium.png">
  <img src="public/images/capture/large.png">

- Page Add Car
  <br>
  <img src="public/images/capture/add.png">

- Page Edit Car
  <br>
  <img src="public/images/capture/edit.png">

- Page Search
  <br>
  <img src="public/images/capture/search.png">


# Info
- Get all cars : GET <code>/api/cars</code>
- Create a car : POST <code>/api/cars/create</code>
- Update a car : PUT <code>/api/cars/update/:id</code>
- Delete a car : DELETE <code>/api/cars/delete/:id</code>

- Filter size car : GET <code>/api/cars/search/?car_size=</code>
- Search cars by name and size : GET <code>/api/cars/search/?q=</code>


# Link Repository

https://gitlab.com/fitriadwiseptiani11/car-management-dasboard-kelompok-6-fsw5

